﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Threading.Tasks;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Responders
{
    public abstract class BaseResponder : IResponder
    {
        protected readonly IPlayerRepository PlayerRepository;
        protected readonly IUnitRepository UnitRepository;
        protected readonly IAirfieldRepository AirfieldRepository;

        protected BaseResponder(IPlayerRepository playerRepository, IUnitRepository unitRepository, IAirfieldRepository airfieldRepository)
        {
            PlayerRepository = playerRepository;
            UnitRepository = unitRepository;
            AirfieldRepository = airfieldRepository;
        }

        public abstract Task<Reply> ProcessTransmission(ITransmission transmission);

        protected static Reply PerformInitialTransmissionChecks(ITransmission transmission)
        {
            var reply = new Reply();

            if (transmission.Intent is ITransmission.Intents.None)
            {
                reply.Notes = "Bot has determined the transmission is not directed to it";
                reply.ContinueProcessing = false;
            } else if (IsUnknown(transmission.Receiver))
            {
                reply.Notes = $"Could not determine who the receiver of the transmission is supposed to be";
                reply.ContinueProcessing = false;
            }

            return reply;
        }

        protected async Task<(Reply, Unit)> DetermineTransmitter(ITransmission transmission, Reply reply, string serverShortName)
        {
            Unit playerUnit = null;

            if (transmission.Transmitter is not ITransmission.Player player || player.IsUnrecognizable())
            {
                reply.CallerCallsign = "last transmitter";
                reply.Message = "i could not recognize your callsign";
                reply.Notes = "Could not find a transmitter callsign in the transmission";
                reply.ContinueProcessing = false;
            }
            else if(!player.IsFullyRecognizable())
            {
                reply.CallerCallsign = player.SpokenWithAmbiguity();
                reply.Message = "i could not fully recognize your callsign";
                reply.Notes = "Could not find a full transmitter callsign in the transmission";
                reply.ContinueProcessing = false;
            }
            else
            {
                try { 
                    reply.CallerCallsign = player.Callsign;
                    playerUnit = await GetTransmitterData(player, serverShortName);
                    // ReSharper disable once InvertIf
                    if (playerUnit == null)
                    {
                        reply.Message = "i could not find you on scope";
                        reply.Notes =
                            "Could not find a live unit on the server being piloted by a player with the provided callsign in their in-game name.";
                        reply.ContinueProcessing = false;
                    }
                } catch (DuplicateCallsignException)
                {
                    reply.Message = "I found more than one unit with a matching callsign";
                    reply.Notes = "The bot will stop procesisng a transmission if a callsign mentioned in the transmission (i.e. transmitter or a referenced friendly) has multiple matching units";
                    reply.ContinueProcessing = false;
                }
            }

            return (reply, playerUnit);
        }

        private async Task<Unit> GetTransmitterData(ITransmission.ITransmitter transmitter,
            string serverShortName)
        {
            var transmittingPlayer = (ITransmission.Player) transmitter;
            return await PlayerRepository.FindByCallsign(transmittingPlayer.GroupName, transmittingPlayer.Flight, 
                transmittingPlayer.Element);
        }

        protected static bool IsUnknown(ITransmission.IReceiver receiver)
        {
            return receiver is ITransmission.UnknownEntity;
        }
    }
}
