﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Geo.Geodesy;
using Geo.Measure;
using RurouniJones.OverlordBot.Core.Atc;
using RurouniJones.OverlordBot.Core.Atc.Extensions;
using RurouniJones.OverlordBot.Datastore.Models;
using RurouniJones.OverlordBot.Encyclopedia;

namespace RurouniJones.OverlordBot.Core.Util
{
    internal static class GeospatialExtensions
    {
        #region Distance To

        public static double DistanceTo(this Airfield.NavigationPoint navigationPoint, Unit otherUnit, DistanceUnit distanceUnit)
        {
            return navigationPoint.Location().CalculateGreatCircleLine(otherUnit.Location).Distance
                .SiValue.ConvertTo(distanceUnit);
        }

        public static double DistanceTo(this Unit unit, Unit otherUnit, DistanceUnit distanceUnit)
        {
            return unit.Location.CalculateGreatCircleLine(otherUnit.Location).Distance
                .SiValue.ConvertTo(distanceUnit);
        }

        public static double DistanceTo(this ControlledAircraft aircraft, Airfield.NavigationPoint navigationPoint,
            DistanceUnit distanceUnit)
        {
            return aircraft.PlayerUnit.Location.CalculateGreatCircleLine(navigationPoint.Location()).Distance
                .SiValue.ConvertTo(distanceUnit);
        }

        public static double DistanceTo(this Airfield.NavigationPoint sourcePoint, Airfield.NavigationPoint destinationPoint,
            DistanceUnit distanceUnit)
        {
            return sourcePoint.Location().CalculateGreatCircleLine(destinationPoint.Location()).Distance
                .SiValue.ConvertTo(distanceUnit);
        }

        #endregion

        #region Bearing To

        public static double BearingTo(this Unit unit, Airfield.NavigationPoint navigationPoint)
        {
            return unit.Location.CalculateGreatCircleLine(navigationPoint.Location())
                .Bearing12;
        }

        public static double BearingTo(this Unit unit, Unit otherUnit)
        {
            return unit.Location.CalculateGreatCircleLine(otherUnit.Location)
                .Bearing12;
        }

        public static double BearingTo(this Airfield.NavigationPoint source, Airfield.NavigationPoint destination)
        {
            return source.Location().CalculateGreatCircleLine(destination.Location())
                .Bearing12;
        }

        #endregion
    }
}