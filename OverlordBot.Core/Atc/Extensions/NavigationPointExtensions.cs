﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using Geo;
using Geo.Geometries;
using RurouniJones.OverlordBot.Encyclopedia;

namespace RurouniJones.OverlordBot.Core.Atc.Extensions
{
    public static class NavigationPointExtensions
    {
        public static Point Location(this Airfield.NavigationPoint navigationPoint)
        {
            return new Point(navigationPoint.Latitude, navigationPoint.Longitude);
        }

        public static double DistanceTo(this Airfield.NavigationPoint navigationPoint, Coordinate otherCoordinate)
        {
            var baseRad = Math.PI * navigationPoint.Latitude / 180;
            var targetRad = Math.PI * otherCoordinate.Latitude / 180;
            var theta = navigationPoint.Longitude - otherCoordinate.Longitude;
            var thetaRad = Math.PI * theta / 180;

            var dist =
                Math.Sin(baseRad) * Math.Sin(targetRad) + Math.Cos(baseRad) *
                Math.Cos(targetRad) * Math.Cos(thetaRad);
            dist = Math.Acos(dist);

            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            return dist;
        }
    }
}