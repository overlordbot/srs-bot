﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Newtonsoft.Json;

namespace RurouniJones.OverlordBot.SimpleRadio.Models
{
    public class ClientInformation
    {
        public int Coalition { get; set; }
        public string Name { get; set; }

        [JsonProperty(PropertyName = "RadioInfo")]
        public RadioInformation RadioInformation { get; set; }

        [JsonProperty(PropertyName = "ClientGuid")]
        public string Guid { get; set; }
    }
}