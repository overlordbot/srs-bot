﻿/* 
Custodian is a DCS server administration tool for Discord
Copyright (C) 2022 Jeffrey Jones

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using RurouniJones.Dcs.Grpc.V0.Coalition;
using RurouniJones.Dcs.Grpc.V0.Trigger;
using RurouniJones.OverlordBot.Core.Awacs.IntentHandlers;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Datastore.Models;
using RurouniJones.OverlordBot.GrpcDatastore;
using static RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions.ITransmission;
using static RurouniJones.OverlordBot.Core.Awacs.AwacsMonitor;

namespace RurouniJones.TextalordBot.Console
{
    public class Monitor
    {
        private readonly ILogger<Monitor> _logger;

        private readonly PlayerRepository _playerRepository;
        private readonly UnitRepository _unitRepository;
        private readonly CoalitionService.CoalitionServiceClient _coalitionClient;
        private readonly TriggerService.TriggerServiceClient _triggerClient;
        private readonly ConcurrentDictionary<string, MonitoredPlayerDetails> _playersMonitored = new();       

        public Monitor(ILogger<Monitor> logger, PlayerRepository playerRepository, UnitRepository unitRepository,
            CoalitionService.CoalitionServiceClient coalitionClient, TriggerService.TriggerServiceClient triggerClient)
        {
            _logger = logger;
            _playerRepository = playerRepository;
            _unitRepository = unitRepository;
            _coalitionClient = coalitionClient;
            _triggerClient = triggerClient;
        }

        public async Task StartMonitoring(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting AWACS Monitoring");
            while (!cancellationToken.IsCancellationRequested)
            {
                _logger.LogTrace("Checking for monitored pilots");
                try
                {
                    await Task.Delay(TimeSpan.FromSeconds(1), CancellationToken.None);
                    foreach (var (id, player) in _playersMonitored)
                    {
                        try
                        {
                            _logger.LogTrace($"Checking for {id} - {player.PlayerCallsign}");
                            var reply = await CheckWarningRadius(player);
                            if (reply == null) continue;

                            var groupResponse = await _coalitionClient.GetGroupsAsync(new GetGroupsRequest
                            {
                                Coalition = (Dcs.Grpc.V0.Common.Coalition)player.PlayerUnit.Coalition + 1
                            });
                            var group = groupResponse.Groups.FirstOrDefault(group => group.Name == player.PlayerUnit.Group);
                            if (group == null) continue;

                            await _triggerClient.OutTextForGroupAsync(new OutTextForGroupRequest()
                            {
                                GroupId = group.Id,
                                Text = reply.Details.ToText(),
                                ClearView = false,
                                DisplayTime = 15
                            }, cancellationToken: cancellationToken);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogWarning(ex, $"Error checking Warning for {id} - {player.PlayerCallsign}");
                            continue;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogWarning("Error checking for monitored pilots",ex);
                    continue;
                }
            }
            _logger.LogInformation("Stopped AWACS Monitoring");
        }

        public void SetWarningRadius(Unit transmittingUnit, int distance)
        {
            var transmittingPlayer = new Player(transmittingUnit.Pilot, 1, 1);

            var monitoredPlayerDetails = new MonitoredPlayerDetails(transmittingPlayer, transmittingUnit, distance);

            if (_playersMonitored.Keys.Contains(monitoredPlayerDetails.PlayerUnit.Id))
            {
                _logger.LogInformation($"Player {monitoredPlayerDetails.PlayerCallsign} already in AWACS monitoring. Removed");
                _playersMonitored.Remove(monitoredPlayerDetails.PlayerUnit.Id, out _);
            }

            _logger.LogInformation($"Player {monitoredPlayerDetails.PlayerCallsign} added to AWACS monitoring");
            _playersMonitored[monitoredPlayerDetails.PlayerUnit.Id] = monitoredPlayerDetails;
        }

        public async Task<Reply> CheckWarningRadius(MonitoredPlayerDetails player)
        {
            if (player == null) return null;

            Unit awacs = null;

            var result = await new WarningRadiusChecker(_playerRepository, _unitRepository).Process(player.PlayerUnit,
                player.WarningDistance, player.MergedContactsReportedToPlayer, player.BoundaryContactsReportedToPlayer, awacs, null, false);

            // Player no longer exists
            if (result.Deleted)
            {
                _logger.LogDebug($"Player {player.PlayerCallsign} removed from AWACS monitoring");
                _playersMonitored.Remove(player.PlayerUnit.Id, out _);
                return null;
            }
            player.MergedContactsReportedToPlayer.AddRange(result.NewMergedContacts);
            player.BoundaryContactsReportedToPlayer.AddRange(result.NewMergedContacts);
            player.BoundaryContactsReportedToPlayer.AddRange(result.NewBoundaryContacts);
            return result.Reply;
        }
    }
}
