﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

namespace RurouniJones.OverlordBot.Discord
{
    public class Configuration
    {
        public static bool EnableDiscordCommands { get; private set; }
        public static void Configure(string discordToken, bool enableCommands = false)
        {
            Client.Instance.BotToken = discordToken;
            EnableDiscordCommands = enableCommands;
        }
    }
}
