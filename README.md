# OverlordBot

## Introduction

OverlordBot is An AWACS Voice bot for Digital Combat Simulator written by
RurouniJones for use with SimpleRadio Standalone (SRS) written by Ciribob.

## Looking for new maintainer

The project is currently unmaintained would welcome a new maintainer to
continue the project.
If you are interested then contact me on the OverlordBot [Discord](https://discord.gg/9RqyTJt)

## Resources

* [Player Guide](https://gitlab.com/overlord-bot/srs-bot/-/wikis/Interaction)
* [Youtube](https://www.youtube.com/channel/UCFDs2Rr9CGOZhVJITT6anCQ)
* [Discord](https://discord.gg/9RqyTJt)
* [Project](https://gitlab.com/overlord-bot)
